﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foundation;
using openFiles.iOS;
using openFiles.Models.OnDevice;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(FilePickerAppleImpl))]
namespace openFiles.iOS
{
    public class FilePickerAppleImpl : IFilePicker
    {
        public Task<string> OpenFileChooser()
        {
            throw new NotImplementedException();
        }

        public string GetResult()
        {
            throw new NotImplementedException();
        }

        public void ClearResult()
        {
            throw new NotImplementedException();
        }

        public string GetFileName()
        {
            throw new NotImplementedException();
        }

        public byte[] GetData()
        {
            throw new NotImplementedException();
        }
    }
}