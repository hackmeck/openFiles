﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using openFiles.iOS;
using openFiles.Models.OnDevice;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(SettingsPersistIOs))]

namespace openFiles.iOS
{
    public class SettingsPersistIOs : ISettingsPersist
    {
        public object GetValue(string key)
        {
            return NSUserDefaults.StandardUserDefaults.StringForKey(key);
        }

        public void SetValue(string key, string value)
        {
            NSUserDefaults.StandardUserDefaults.SetString(value, key);
        }
    }
}