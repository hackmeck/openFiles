﻿using System.Collections.ObjectModel;
using System.Linq;
using openFiles.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace openFiles.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SendPage : ContentPage
    {
        private readonly ObservableCollection<UiItem> _items = new ObservableCollection<UiItem>();

        public SendPage()
        {
            InitializeComponent();

            BindingContext = new SendPageViewModel(DevicePicker, _items);
            ItemList.ItemsSource = _items;
            if (!ItemList.GestureRecognizers.Any())
            {
                ItemList.GestureRecognizers.Add(new TapGestureRecognizer());
            }
        }
    }

    public class UiItem
    {
        public UiItem(string fileName)
        {
            DisplayName = fileName;
            SendIt = true;
        }

        public string DisplayName { get; set; }

        public bool SendIt { get; set; }
    }
}