﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace openFiles.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }
    }
}