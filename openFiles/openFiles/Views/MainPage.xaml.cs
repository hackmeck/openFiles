﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace openFiles.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = this;
        }
    }
}