﻿using openFiles.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace openFiles.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OptionsPage : ContentPage
    {
        public OptionsPage()
        {
            InitializeComponent();
            BindingContext = new OptionsPageViewModel();
        }
    }
}