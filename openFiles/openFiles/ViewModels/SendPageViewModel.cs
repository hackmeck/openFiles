﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using openFiles.Models;
using openFiles.Models.OnDevice;
using openFiles.Transmitter;
using openFiles.Transmitter.Model;
using openFiles.Views;
using Xamarin.Forms;

namespace openFiles.ViewModels
{
    public class SendPageViewModel : BaseViewModel
    {
        private readonly Picker _devicePicker;
        private readonly Dictionary<UiItem, TransmitterFile> _files = new Dictionary<UiItem, TransmitterFile>();

        private readonly ObservableCollection<UiItem> _filesToSend;
        private bool _canSend;
        private int _selectedDviceNumber;
        private TransmitterUser _selectedUser;
        private ITransmitter _transmitter;
        private ICollection<TransmitterUser> _users;


        public SendPageViewModel(Picker devicePicker, ObservableCollection<UiItem> observableCollection)
        {
            //User initalisieren, DevicePicker füllen, selectedDeviceNumber inizalisieren, selectedUserInitalisieren
            Instance = this;
            CanSend = false;
            RefreshTransmitter();

            _filesToSend = observableCollection;
            _filesToSend.CollectionChanged += OnCollectionChange;
            _devicePicker = devicePicker;
            _selectedDviceNumber = -1;

            SendCommand = new Command(Send);
            AddFileCommand = new Command(AddFile);
            RemoveFileCommand = new Command(RemoveFile);
            RefreshListCommand = new Command(FillPicker);
        }

        public static SendPageViewModel Instance { get; private set; }

        public bool CanSend
        {
            get => _canSend && IsNotBusy && _selectedUser != null;
            set
            {
                if (value == _canSend) return;
                _canSend = value;
                OnPropertyChanged();
            }
        }

        public ICommand SendCommand { get; set; }

        public ICommand AddFileCommand { get; set; }

        public ICommand RemoveFileCommand { get; set; }


        public ICommand RefreshListCommand { get; set; }

        public int SelectedDeviceNumber
        {
            get => _selectedDviceNumber;
            set
            {
                if (value == _selectedDviceNumber) return;
                _selectedDviceNumber = value;
                OnPropertyChanged();
                if (_devicePicker.Items.Count <= 0) return;
                var userName = _devicePicker.Items.ToArray()[value];
                _selectedUser = _users.FirstOrDefault(user => user.ToPresentation() == userName);

                // ReSharper disable once ExplicitCallerInfoArgument
                OnPropertyChanged("CanSend");
            }
        }

        public void CloseConnections()
        {
            _transmitter?.CloseConnections();
        }

        public void StartTransmitter()
        {
            if (_transmitter == null) _transmitter = TransmitterFactory.BuildTransmitter();

            RefreshTransmitter();
        }


        public void RefreshTransmitter()
        {
            if (_transmitter == null) return;
            _transmitter?.CloseConnections();
            _transmitter = TransmitterFactory.BuildTransmitter();
            _transmitter?.Recieve((user, file) =>
            {
                try
                {
                    if (ConstValues.Platform == ConstValues.DevicePlatform.Uwp)
                        DependencyService.Get<IFileSaver>().OpenSaveDialog(file);
                    else
                        DependencyService.Get<IFileSaver>().SaveFile(file, user);
                }
                catch (Exception ex)
                {
                    Logger.Log("Error while saving file: " + ex, Logger.LoggingCategory.Error);
                }
            });
        }

        private void RemoveFile()
        {
            foreach (var uiFile in _filesToSend.Reverse().Where(x => !x.SendIt))
            {
                _files.Remove(uiFile);
                _filesToSend.Remove(uiFile);
            }

            GC.Collect();
        }

        private async void AddToFileList()
        {
            IsBusy = true;
            var fileName = DependencyService.Get<IFilePicker>().GetFileName();
            if (string.IsNullOrEmpty(fileName))
            {
                IsBusy = false;
                return;
            }

            var uiItem = new UiItem(fileName);
            await Task.Run(() =>
            {
                var data = DependencyService.Get<IFilePicker>().GetData();
                var fileToAdd = new TransmitterFile(fileName, data);
                //TODO we should handle the sending completly by streams, so we don't over exceed our memory
                DependencyService.Get<IFilePicker>().ClearResult();
                GC.Collect();
                _files.Add(uiItem, fileToAdd);
            });
            _filesToSend.Add(uiItem);
            IsBusy = false;
        }

        private async void Send()
        {
            ICollection<TransmitterFile> toSend = new LinkedList<TransmitterFile>();
            foreach (var uiFile in _filesToSend.Where(x => x.SendIt))
            {
                _files.TryGetValue(uiFile, out var singleFileToSend);
                toSend.Add(singleFileToSend);
            }

            if (_selectedUser == null) return;
            IsBusy = true;
            var fut = toSend.Select(file => _transmitter.Send(_selectedUser, file));
            foreach (var f in fut) await f;

            IsBusy = false;
            await Application.Current.MainPage.DisplayAlert("Sending Complete", "", "OK");
        }

        private async void FillPicker()
        {
            Logger.Log("pressed refresh", Logger.LoggingCategory.Trace);
            IsBusy = true;
            await Task.Run(() => { _users = _transmitter.FindRecievers(); });
            IsBusy = false;
            _devicePicker.Items.Clear();
            foreach (var user in _users) _devicePicker.Items.Add(user.ToPresentation());
        }

        private async void AddFile()
        {
            await DependencyService.Get<IFilePicker>().OpenFileChooser();
            if (Device.RuntimePlatform == Device.UWP) AddToFileList();
        }

        public void Refresh()
        {
            if (Device.RuntimePlatform == Device.Android) AddToFileList();
        }


        private void OnCollectionChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            CanSend = _filesToSend.Count != 0;
            // ReSharper disable once ExplicitCallerInfoArgument
            OnPropertyChanged("FilesToSend");
        }
    }
}