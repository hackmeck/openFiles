﻿using openFiles.Models;
using openFiles.Transmitter;

namespace openFiles.ViewModels
{
    public class OptionsPageViewModel : BaseViewModel
    {
        private string _name;
        private int _port;
        private bool _visible;
        private bool _receive;

        public OptionsPageViewModel()
        {
            _port = Persistence.LoadValue(ConstValues.PropertyPort, ConstValues.DefaultPort);
            _name = Persistence.LoadValue(ConstValues.PropertyDeviceName, ConstValues.DefaultDeviceName);
            _visible = Persistence.LoadValue(ConstValues.PropertyVisible, ConstValues.DefaultVisible);
            _receive = Persistence.LoadValue(ConstValues.PropertyReceive, ConstValues.DefaultReceive);
            RefreshUser();
        }

        public string LocalIpText => "local IP: " + Util.GetOwnIp();

        public int Port
        {
            get => _port;
            set
            {
                if (value == _port) return;
                _port = value;
                OnPropertyChanged();
                Persistence.SaveValue(ConstValues.PropertyPort, value);
                RefreshUser();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
                Persistence.SaveValue(ConstValues.PropertyDeviceName, value);
                RefreshUser();
            }
        }

        public bool Visible
        {
            get => _visible;
            set
            {
                if (value == _visible) return;
                _visible = value;
                OnPropertyChanged();
                Persistence.SaveValue(ConstValues.PropertyVisible, value);
                RefreshUser();
            }
        }

        public bool Receive
        {
            get => _receive;
            set
            {
                if (value == _receive) return;
                _receive = value;
                OnPropertyChanged();
                Persistence.SaveValue(ConstValues.PropertyReceive, value);
                RefreshUser();
            }
        }

        private static void RefreshUser()
        {
            SendPageViewModel.Instance.RefreshTransmitter();
        }
    }
}