﻿using System;
using System.Windows.Input;
using openFiles.Models;
using Xamarin.Forms;

namespace openFiles.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            OpenWebCommand = new Command(() => Device.OpenUri(new Uri(ConstValues.GitLabLink)));
        }

        public ICommand OpenWebCommand { get; }
    }
}