﻿#region TRACE_SETTING

//switch if the Transmitter should trace it#s connections to the Logger
#define TRACE_CONNECT
//#undef TRACE_CONNECT

#if !DEBUG
#undef TRACE_CONNECT
#endif

#endregion

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using openFiles.Models;
using openFiles.Transmitter.Model;
using Xamarin.Forms;

namespace openFiles.Transmitter
{
    public class Transmitter : ITransmitter
    {
        private const string SendFile = "sending";
        private const string SayName = "hello c:";
        private const string Ok = "Ok";
        private const string Error = "Err";
        private readonly TcpListener _listener;


        private readonly TransmitterUser _me;
        private readonly bool _visible;
        private readonly bool _receive;
        private TcpClient _lastClient;
        private Task _recieverTask;
        private bool _recive;

        public Transmitter(TransmitterUser user, bool visible, bool receive)
        {
            _me = user;
            _listener = new TcpListener(user.Ip, user.Port);
            _visible = visible;
            _receive = receive;
        }


        public void Recieve(RecieveOperation recieveOperation)
        {
            _recive = true;
            try
            {
                _listener.Start();

                #region TRACE

#if TRACE_CONNECT
                Logger.Log("Transmitter listener Task started", Logger.LoggingCategory.Trace);
#endif

                #endregion

                _recieverTask = Task.Run(() =>
                {
                    try
                    {
                        while (_recive)
                        {
                            var client = _lastClient = _listener.AcceptTcpClient();

                            #region TRACE

#if TRACE_CONNECT
                            Logger.Log(
                                "CLient with IP " + (client.Client.RemoteEndPoint as IPEndPoint)?.Address +
                                " connected",
                                Logger.LoggingCategory.Trace);
#endif

                            #endregion

                            InternallyRecieve(client, recieveOperation);

                            #region TRACE

#if TRACE_CONNECT
                            Logger.Log(
                                "CLient with IP " + (client.Client.RemoteEndPoint as IPEndPoint)?.Address +
                                " disconnected",
                                Logger.LoggingCategory.Trace);
#endif

                            #endregion

                            client.Close();
                            GC.Collect();
                        }
                    }
                    catch (SocketException e)
                    {
                        if (e.SocketErrorCode != SocketError.Interrupted)
                            Logger.Log("listener Task got socket exception " + e.Message,
                                Logger.LoggingCategory.Warning);
                    }

                    #region TRACE

#if TRACE_CONNECT
                    finally
                    {
                        Logger.Log("Transmitter Task shut down", Logger.LoggingCategory.Trace);
                    }
#endif

                    #endregion
                });
            }
            catch (SocketException ex)
            {
                Logger.Log("couldn't start listener " + ex.Message, Logger.LoggingCategory.Error);
#if DEBUG
                Application.Current.MainPage.DisplayAlert("Couldn't open Listener on Port " + _me.Port,
                    "Reason: " + ex.Message, "OK");
#else
                App.Current.MainPage.DisplayAlert("Couldn't open Listener on Port " + _me.Port, "", "OK");
#endif
            }
        }

        public async Task Send(TransmitterUser targetUser, TransmitterFile data)
        {
            var socket = new TcpClient();
            await socket.ConnectAsync(targetUser.Ip, targetUser.Port);
            Util.SendStringViaStream(ConstValues.Version, socket.GetStream());
            if (Util.ReadStringFromStream(socket.GetStream()) == Ok)
            {
                Util.SendStringViaStream(SendFile, socket.GetStream());
            }
            else
            {
                socket.Close();
                return;
            }

            if (Util.ReadStringFromStream(socket.GetStream()) == Ok)
            {
                Util.SendStringViaStream(_me.Name, socket.GetStream());
            }
            else
            {
                socket.Close();
                return;
            }

            if (Util.ReadStringFromStream(socket.GetStream()) == Ok)
            {
                Util.SendStringViaStream(data.Name, socket.GetStream());
            }
            else
            {
                socket.Close();
                return;
            }

            if (Util.ReadStringFromStream(socket.GetStream()) == Ok)
                Util.SendBytesViaStream(data.Data, socket.GetStream());

            if (socket.Connected) socket.Close();

            #region TRACE

#if TRACE_CONNECT
            Logger.Log("Succesfully send file to " + targetUser, Logger.LoggingCategory.Trace);
#endif

            #endregion
        }

        public void CloseConnections()
        {
            if (!_recive) return;
            _recive = false;
            _listener.Stop();

            #region TRACE

#if TRACE_CONNECT
            Logger.Log("waiting for reciever to exit", Logger.LoggingCategory.Trace);
#endif

            #endregion

            _recieverTask.Wait();

            #region TRACE

#if TRACE_CONNECT
            Logger.Log("reciver exited", Logger.LoggingCategory.Trace);
#endif

            #endregion

            if (_lastClient != null && _lastClient.Connected) _lastClient.Close();
        }

        public ICollection<TransmitterUser> FindRecievers()
        {
            var connection = new KeyValuePair<TcpClient, Task>[254];
            //xxx.xxx.xxx.xxx
            var ipSearch = getSubnet(_me.Ip.ToString());
            var foundUsers = new LinkedList<TransmitterUser>();

            for (var i = 0; i < 254; i++)
            {
                //var localEndPoint = new IPEndPoint(IPAddress.Any, );
                var socket = new TcpClient(AddressFamily.InterNetwork);
                var ipToConnet = ipSearch + (i + 1);
                if (ipToConnet == _me.Ip.ToString()) continue;
                var awater = socket.ConnectAsync(IPAddress.Parse(ipToConnet), _me.Port);
                connection[i] = new KeyValuePair<TcpClient, Task>(socket, awater);
            }


            for (var i = 0; i < 254; i++)
            {
                var ipToConnet = ipSearch + (i + 1);
                if (ipToConnet == _me.Ip.ToString()) continue;

                try
                {
                    var found = connection[i].Value.Wait(i == 0 ? ConstValues.ConnectionTimeOut : 25);
                    if (found)
                    {
                        var socket = connection[i].Key;

                        #region TRACE

#if TRACE_CONNECT
                        Logger.Log("Found endpoint at " + (socket.Client.RemoteEndPoint as IPEndPoint)?.Address,
                            Logger.LoggingCategory.Trace);
#endif

                        #endregion


                        Util.SendStringViaStream(ConstValues.Version, socket.GetStream());
                        var transmissionResult = Util.ReadStringFromStream(socket.GetStream());
                        if (transmissionResult != Ok)
                        {
                            socket.Dispose();
                            continue;
                        }

                        Util.SendStringViaStream(SayName, socket.GetStream());
                        if (Util.ReadStringFromStream(socket.GetStream()) != Ok)
                        {
                            socket.Dispose();
                            continue;
                        }
                        var name = Util.ReadStringFromStream(socket.GetStream());

                        foundUsers.AddLast(new TransmitterUser((socket.Client.RemoteEndPoint as IPEndPoint)?.Address,
                            _me.Port, name));
                        socket.Dispose();
                        connection[i].Key.Close();
                    }
                }
                catch (Exception)
                {
                    //couldn't connect to ip:port -> ignore
                }
            }

            return foundUsers;
        }

        private void InternallyRecieve(TcpClient socket, RecieveOperation operation)
        {
            var result = Util.ReadStringFromStream(socket.GetStream());
            if (result == ConstValues.Version)
            {
                Util.SendStringViaStream(Ok, socket.GetStream());
            }
            else
            {
                Util.SendStringViaStream(Error, socket.GetStream());
                return;
            }

            var whatClientWants = Util.ReadStringFromStream(socket.GetStream());
            switch (whatClientWants)
            {
                case SendFile:

                    #region TRACE

#if TRACE_CONNECT
                    Logger.Log(
                        "Client with IP " + (socket.Client.RemoteEndPoint as IPEndPoint)?.Address +
                        " wants to send a file", Logger.LoggingCategory.Trace);
#endif

                    #endregion
                    if (!_receive)
                    {
                        Util.SendStringViaStream(Error, socket.GetStream());
                    } else
                    {
                        RecieveFile(socket, operation);
                    }
                    break;
                case SayName:

                    #region TRACE

#if TRACE_CONNECT
                    Logger.Log(
                        "Client with IP " + (socket.Client.RemoteEndPoint as IPEndPoint)?.Address +
                        " wants to know our name", Logger.LoggingCategory.Trace);
#endif

                    #endregion
                    if (!_visible)
                    {
                        Util.SendStringViaStream(Error, socket.GetStream());
                    }
                    else
                    {
                        Util.SendStringViaStream(Ok, socket.GetStream());
                        Util.SendStringViaStream(_me.Name, socket.GetStream());
                    }
                    break;
                default:
                    Logger.Log(
                        "Client with IP " + (socket.Client.RemoteEndPoint as IPEndPoint)?.Address +
                        " wants an not supported action: " + whatClientWants, Logger.LoggingCategory.Warning);
                    Util.SendStringViaStream(Error, socket.GetStream());
                    break;
            }
        }

        private static void RecieveFile(TcpClient socket, RecieveOperation operation)
        {
            Util.SendStringViaStream(Ok, socket.GetStream());
            var userName = Util.ReadStringFromStream(socket.GetStream());
            var clientUser = new TransmitterUser((socket.Client.RemoteEndPoint as IPEndPoint)?.Address,
                ((IPEndPoint) socket.Client.RemoteEndPoint).Port, userName);

            Util.SendStringViaStream(Ok, socket.GetStream());
            var fileName = Util.ReadStringFromStream(socket.GetStream());
            Util.SendStringViaStream(Ok, socket.GetStream());
            var data = Util.ReadBytesFromStream(socket.GetStream());
            var file = new TransmitterFile(fileName, data);

            #region TRACE

#if TRACE_CONNECT
            Logger.Log("Succesfully recieved " + file + " from " + clientUser, Logger.LoggingCategory.Trace);
#endif

            #endregion

            operation.Invoke(clientUser, file);
        }

        private string getSubnet(string ip)
        {
            var tmp = ip.Split('.');
            //xxx.xxx.xxx.
            return tmp[0] + "." + tmp[1] + "." + tmp[2] + ".";
        }
    }
}