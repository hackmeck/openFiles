﻿using System.Collections.Generic;
using System.Threading.Tasks;
using openFiles.Transmitter.Model;

namespace openFiles.Transmitter
{
    public interface ITransmitter
    {
        void Recieve(RecieveOperation recieveOperation);

        Task Send(TransmitterUser targetUser, TransmitterFile data);

        ICollection<TransmitterUser> FindRecievers();

        void CloseConnections();
    }

    public delegate void RecieveOperation(TransmitterUser user, TransmitterFile file);
}