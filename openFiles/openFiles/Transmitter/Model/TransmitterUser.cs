﻿using System.Net;

namespace openFiles.Transmitter.Model
{
    public class TransmitterUser
    {
        public TransmitterUser(IPAddress ip, int port, string name)
        {
            Ip = ip;
            Port = port;
            Name = name;
        }

        public IPAddress Ip { get; }
        public int Port { get; }
        public string Name { get; }

        public string ToPresentation()
        {
            return Name + "@" + Ip;
        }

        public override string ToString()
        {
            return ToPresentation() + ":" + Port;
        }
    }
}