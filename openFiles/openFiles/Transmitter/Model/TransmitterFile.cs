﻿namespace openFiles.Transmitter.Model
{
    public class TransmitterFile
    {
        public TransmitterFile(string name, byte[] data)
        {
            Name = name;
            Data = data;
        }

        public string Name { get; }

        public string Ending => Name.Split('.')[1];
        public byte[] Data { get; }

        public string NameWithoutEnding => Name.Split('.')[0];

        public override string ToString()
        {
            return Name;
        }
    }
}