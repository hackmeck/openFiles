﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using openFiles.Models;

namespace openFiles.Transmitter
{
    public static class Util
    {
        public static IPAddress GetOwnIp()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                    return ip;

            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public static void SendStringViaStream(string toSend, Stream stream)
        {
            var writeBuffer = Encoding.UTF8.GetBytes(toSend);
            SendBytesViaStream(writeBuffer, stream);
        }

        public static byte[] ReadBytesFromStream(Stream stream)
        {
            var intBuffer = new byte[sizeof(int)];
            stream.Read(intBuffer, 0, sizeof(int));
            var sizeOfBuffer = BitConverter.ToInt32(intBuffer, 0);

            var buffer = new byte[sizeOfBuffer];
            var bytesRead = stream.Read(buffer, 0, buffer.Length);
            while (bytesRead < sizeOfBuffer)
            {
                
                bytesRead += stream.Read(buffer, bytesRead, buffer.Length - bytesRead);
                Logger.Log("read " + bytesRead + " bytes of " + sizeOfBuffer + " (" + (bytesRead * 100) / sizeOfBuffer + "%)", Logger.LoggingCategory.Debug);
            }
            return buffer;
        }

        public static string ReadStringFromStream(Stream stream)
        {
            var buffer = ReadBytesFromStream(stream);
            return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
        }

        public static void SendBytesViaStream(byte[] toSend, Stream stream)
        {
            //size of sending byte array as an 4 byte integer
            var size = BitConverter.GetBytes(toSend.Length);
            stream.Write(size, 0, sizeof(int));
            stream.Flush();
            stream.Write(toSend, 0, toSend.Length);
            stream.Flush();
        }
    }
}