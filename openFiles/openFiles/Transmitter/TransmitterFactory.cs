﻿using openFiles.Models;
using openFiles.Transmitter.Model;

namespace openFiles.Transmitter
{
    public static class TransmitterFactory
    {
        public static ITransmitter BuildTransmitter()
        {
            var port = Persistence.LoadValue(ConstValues.PropertyPort, ConstValues.DefaultPort);
            var name = Persistence.LoadValue(ConstValues.PropertyDeviceName, ConstValues.DefaultDeviceName);
            var visible = Persistence.LoadValue(ConstValues.PropertyVisible, ConstValues.DefaultVisible);
            var receive = Persistence.LoadValue(ConstValues.PropertyReceive, ConstValues.DefaultReceive);
            var user = new TransmitterUser(Util.GetOwnIp(), port, name);
            return new Transmitter(user, visible, receive);
        }
    }
}