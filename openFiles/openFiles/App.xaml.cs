﻿using openFiles.Models;
using openFiles.ViewModels;
using openFiles.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace openFiles
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Current.Resources["Version"] = ConstValues.Version;
            if (ConstValues.Platform == ConstValues.DevicePlatform.Uwp)
                Current.Resources["FontSize"] = 18d;
            else
            {
                Current.Resources["FontSize"] = 20d;
                Current.Resources["BarBackgroundColor"] = Current.Resources["BarBackgroundColorAndroid"];
            }

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            SendPageViewModel.Instance.StartTransmitter();
        }

        protected override void OnSleep()
        {
            SendPageViewModel.Instance.CloseConnections();
        }

        protected override void OnResume()
        {
            SendPageViewModel.Instance.RefreshTransmitter();
            SendPageViewModel.Instance.Refresh();
        }
    }
}