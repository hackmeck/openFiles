﻿#region Enable_Disable_Load_Save_Trace

//use this to activate/deactivate tracing load saves
//#define TRACE_SETTING_LOAD_SAVE

//don't trace if not debugging
#if !DEBUG
#undef TRACE_SETTING_LOAD_SAVE
#endif

#endregion

using System;
using openFiles.Models.OnDevice;
using Xamarin.Forms;

namespace openFiles.Models
{
    public static class Persistence
    {
        public static T LoadValue<T>(string key, T defaultValue)
        {
            if (typeof(T) == typeof(int))
                return InternalLoadValue(key, defaultValue, x => (T) (object) int.Parse(x.ToString()));

            if (typeof(T) == typeof(string))
                return InternalLoadValue(key, defaultValue, x => (T) (object) x.ToString());

            if (typeof(T) == typeof(float))
                return InternalLoadValue(key, defaultValue, x => (T) (object) float.Parse(x.ToString()));

            if (typeof(T) == typeof(double))
                return InternalLoadValue(key, defaultValue, x => (T) (object) double.Parse(x.ToString()));

            if (typeof(T) == typeof(bool))
                return InternalLoadValue(key, defaultValue, x => (T) (object) bool.Parse(x.ToString()));

            return InternalLoadValue(key, defaultValue, x => (T) x);
        }

        private static T InternalLoadValue<T>(string key, T defaultValue, Func<object, T> parser)
        {
            try
            {
                var x = DependencyService.Get<ISettingsPersist>().GetValue(key);

                #region TRACE

#if TRACE_SETTING_LOAD_SAVE
                Logger.Log("loaded setting with key " + key + " with saved value: " + x,
                    Logger.LoggingCategory.Trace);
#endif

                #endregion

                var z = x != null ? parser.Invoke(x) : defaultValue;
                return string.IsNullOrWhiteSpace(z.ToString()) ? defaultValue : z;
            }
            catch (Exception ex)
            {
                Logger.Log("couldn't load " + key + " from settings", Logger.LoggingCategory.Warning);
                Logger.Log(ex, Logger.LoggingCategory.Warning);
                return defaultValue;
            }
        }

        public static void SaveValue<T>(string key, T value)
        {
            try
            {
                DependencyService.Get<ISettingsPersist>().SetValue(key, value.ToString());

                #region TRACE

#if TRACE_SETTING_LOAD_SAVE
                Logger.Log("saved value with key " + key + " with new value: " + value, Logger.LoggingCategory.Trace);
#endif

                #endregion
            }
            catch (Exception ex)
            {
                Logger.Log("couldn't save " + key + " to settings", Logger.LoggingCategory.Warning);
                Logger.Log(ex, Logger.LoggingCategory.Warning);
            }
        }
    }
}