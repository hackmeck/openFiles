﻿namespace openFiles.Models.OnDevice
{
    public interface ISettingsPersist
    {
        object GetValue(string key);

        void SetValue(string key, string value);
    }
}