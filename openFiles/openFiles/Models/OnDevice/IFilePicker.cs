﻿using System.Threading.Tasks;

namespace openFiles.Models.OnDevice
{
    public interface IFilePicker
    {
        /// <summary>
        ///     This method opens an async file chooser and returns the result as a Task{string}.
        /// </summary>
        /// <returns>The result of the file chooser (the path of the selected file).</returns>
        Task<string> OpenFileChooser();

        /// <summary>
        ///     This method resets the file chooser.
        /// </summary>
        void ClearResult();

        /// <summary>
        ///     This method returns the name of the selected file.
        /// </summary>
        /// <returns>The name of the selected file.</returns>
        string GetFileName();

        /// <summary>
        ///     This method returns the file ending of the selected file.
        /// </summary>
        /// <returns>The file ending of the selected file.</returns>
        byte[] GetData();
    }
}