﻿using openFiles.Transmitter.Model;

namespace openFiles.Models.OnDevice
{
    public interface IFileSaver
    {
        void SaveFile(TransmitterFile file, TransmitterUser user);

        void OpenSaveDialog(TransmitterFile file);
    }
}