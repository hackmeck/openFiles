﻿using System;
using Xamarin.Forms;

namespace openFiles.Models
{
    public static class ConstValues
    {
        public enum DevicePlatform
        {
            Android,
            Uwp,
            Ios
        }

        public const string Version = "Alpha_0.3.1";
        public const string GitLabLink = "https://gitlab.com/hackmeck/openFiles";

        public const int ConnectionTimeOut = 500;

        public const string PropertyDeviceName = "device-name";
        public const string DefaultDeviceName = "I´m a Device";

        public const string PropertyPort = "port";
        public const int DefaultPort = 12334;

        public const string PropertyVisible = "visible";
        public const bool DefaultVisible = true;

        public const string PropertyReceive = "receive";
        public const bool DefaultReceive = true;

        //only logging events with verbosity <= LoggerVerbosity will get logged
        //mind that if comiled for release nothing gets logged
        public const Logger.LoggingCategory LoggerVerbosity = Logger.LoggingCategory.Trace;

        public static DevicePlatform Platform
        {
            get
            {
                if (Device.RuntimePlatform == Device.Android) return DevicePlatform.Android;
                if (Device.RuntimePlatform == Device.UWP) return DevicePlatform.Uwp;

                if (Device.RuntimePlatform == Device.iOS) return DevicePlatform.Ios;
                throw new NotImplementedException("the current platform is not supported");
            }
        }
    }
}