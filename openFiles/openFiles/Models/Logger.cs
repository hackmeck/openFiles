﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace openFiles.Models
{
    public static class Logger
    {
        public enum LoggingCategory
        {
            //mind that the order here is important for the logging verbosity
            Error,
            Warning,
            Debug,
            Trace
        }

        //won't log anything if compiled for release
        public static void Log(object message, LoggingCategory category, [CallerMemberName] string callerName = "",
            [CallerFilePath] string callerFilePath = "",
            [CallerLineNumber] int callerLine = 0)
        {
#if DEBUG
            if (category > ConstValues.LoggerVerbosity) return;
            var prefix = "[" + category + "] " + callerName + "(" + callerLine + ")";
            Debug.WriteLine(message, prefix);
#endif
        }
    }
}