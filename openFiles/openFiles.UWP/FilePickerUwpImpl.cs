﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using openFiles.Models.OnDevice;
using openFiles.UWP;
using Xamarin.Forms;

[assembly: Dependency(typeof(FilePickerUwpImpl))]

namespace openFiles.UWP
{
    public class FilePickerUwpImpl : IFilePicker
    {
        private StorageFile _file;

        public async Task<string> OpenFileChooser()
        {
            var picker =
                new FileOpenPicker
                {
                    ViewMode = PickerViewMode.Thumbnail,
                    SuggestedStartLocation = PickerLocationId.Desktop
                };
            picker.FileTypeFilter.Add("*");

            _file = await picker.PickSingleFileAsync();

            return _file?.Path ?? "couldn't open file";
        }

        public void ClearResult()
        {
            _file = null;
        }

        public byte[] GetData()
        {
            if (_file == null) return null;
            var buffer = FileIO.ReadBufferAsync(_file).AsTask().Result;
            return buffer.ToArray();
        }


        public string GetFileName()
        {
            return _file?.Name;
        }
    }
}