﻿using Windows.Storage;
using openFiles.Models.OnDevice;
using openFiles.UWP;
using Xamarin.Forms;

[assembly: Dependency(typeof(SettingsPersistUwpImpl))]

namespace openFiles.UWP
{
    public class SettingsPersistUwpImpl : ISettingsPersist
    {
        public object GetValue(string key)
        {
            var localSettings = ApplicationData.Current.LocalSettings;
            return localSettings.Values[key];
        }

        public void SetValue(string key, string value)
        {
            var localSettings = ApplicationData.Current.LocalSettings;
            localSettings.Values[key] = value;
        }
    }
}