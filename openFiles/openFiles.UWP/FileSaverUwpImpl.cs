﻿using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Core;
using openFiles.Models;
using openFiles.Models.OnDevice;
using openFiles.Transmitter.Model;
using openFiles.UWP;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileSaverUwpImpl))]

namespace openFiles.UWP
{
    public class FileSaverUwpImpl : IFileSaver
    {
        public async void SaveFile(TransmitterFile filePar, TransmitterUser user)
        {
            var localFolder = ApplicationData.Current.LocalFolder;
            var sampleFile = await localFolder.CreateFileAsync(filePar.Name, CreationCollisionOption.ReplaceExisting);
            WriteFileToStorageFile(filePar, sampleFile);
        }

        public async void OpenSaveDialog(TransmitterFile file)
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.High, async () =>
            {
                var savePicker = new FileSavePicker
                {
                    SuggestedStartLocation = PickerLocationId.Downloads
                };

                // Dropdown of file types the user can save the file as
                savePicker.FileTypeChoices.Add("Default", new List<string> {"." + file.Ending});
                // Default file name if the user does not type one in or select a file to replace
                savePicker.SuggestedFileName = file.NameWithoutEnding;
                var storageFile = await savePicker.PickSaveFileAsync();
                if (storageFile != null)
                {
                    WriteFileToStorageFile(file, storageFile);
                }
            });
        }

        private static async void WriteFileToStorageFile(TransmitterFile fileToSave, StorageFile dest)
        {
            CachedFileManager.DeferUpdates(dest);
            await FileIO.WriteBytesAsync(dest, fileToSave.Data);
            var status = await CachedFileManager.CompleteUpdatesAsync(dest);
            Logger.Log("file was saved with status " + status, Logger.LoggingCategory.Debug);
            await CachedFileManager.CompleteUpdatesAsync(dest);
        }
    }
}