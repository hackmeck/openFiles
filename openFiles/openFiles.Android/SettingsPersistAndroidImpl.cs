﻿using Android.Content;
using openFiles.Droid;
using openFiles.Models.OnDevice;
using Xamarin.Forms;

[assembly: Dependency(typeof(SettingsPersistAndroidImpl))]

namespace openFiles.Droid
{
    public class SettingsPersistAndroidImpl : ISettingsPersist
    {
        public object GetValue(string key)
        {
            var loaded = MainActivity.Instance.GetPreferences(FileCreationMode.Private).GetString(key, string.Empty);
            return loaded;
        }

        public void SetValue(string key, string value)
        {
            var edit = MainActivity.Instance.GetPreferences(FileCreationMode.Private).Edit();
            edit?.PutString(key, value);
            edit?.Apply();
        }
    }
}