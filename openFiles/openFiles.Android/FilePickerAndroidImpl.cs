﻿using System.Threading.Tasks;
using Android.Content;
using openFiles.Droid;
using openFiles.Models.OnDevice;
using Xamarin.Forms;

[assembly: Dependency(typeof(FilePickerAndroidImpl))]

namespace openFiles.Droid
{
    public class FilePickerAndroidImpl : IFilePicker
    {
        public Task<string> OpenFileChooser()
        {
            return Task.Run(() => OpenFileChoser());
        }


        public void ClearResult()
        {
            MainActivity.ClearMsg();
        }

        public string GetFileName()
        {
            return MainActivity.GetName();
        }

        public byte[] GetData()
        {
            return MainActivity.GetBytes();
        }

        private static string OpenFileChoser()
        {
            var intent = new Intent(Intent.ActionGetContent);
            intent.SetType("*/*");
            MainActivity.OpenMediaChooser(intent);
            return MainActivity.GetResult();
        }
    }
}