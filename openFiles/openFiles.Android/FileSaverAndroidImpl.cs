﻿using System;
using Java.IO;
using openFiles.Droid;
using openFiles.Models;
using openFiles.Models.OnDevice;
using openFiles.Transmitter.Model;
using Xamarin.Forms;
using Environment = Android.OS.Environment;

[assembly: Dependency(typeof(FileSaverAndroidImpl))]

namespace openFiles.Droid
{
    public class FileSaverAndroidImpl : IFileSaver
    {
        public void SaveFile(TransmitterFile file, TransmitterUser user)
        {
            if (IsExternalStorageWritable())
            {
                //TODO we could set an option here for the target path/ folder (now it is in Downloads/sending user.name/
                var javaFile = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryDownloads),
                    user.Name);
                if (!javaFile.Mkdirs()) Logger.Log("Directory not created", Logger.LoggingCategory.Debug);
                var outputStream = new FileOutputStream(javaFile.AbsoluteFile + "/" + file.Name);
                outputStream.Write(file.Data);
                outputStream.Close();
                javaFile.Dispose();
            }
            else
            {
                Logger.Log("External Storage not awailable for write", Logger.LoggingCategory.Error);
            }
        }

        public void OpenSaveDialog(TransmitterFile file)
        {
            throw new NotImplementedException();
        }

        private static bool IsExternalStorageWritable()
        {
            var state = Environment.ExternalStorageState;
            return Environment.MediaMounted == state;
        }
    }
}