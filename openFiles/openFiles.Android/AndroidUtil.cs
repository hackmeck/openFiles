﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Database;
using Android.Net;
using Android.OS;
using Android.Provider;
using Android.Support.V4.App;
using Android.Support.V4.Content;

namespace openFiles.Droid
{
    public static class AndroidUtil
    {
        public static string GetPath(Context context, Uri uri)
        {
            var isKitKat = Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Kitkat;

            // DocumentProvider
            if (isKitKat && DocumentsContract.IsDocumentUri(context, uri))
            {
                // ExternalStorageProvider
                if (IsExternalStorageDocument(uri))
                {
                    var docId = DocumentsContract.GetDocumentId(uri);
                    var split = docId.Split(':');
                    var type = split[0];


                    return Environment.GetExternalStoragePublicDirectory(type).AbsolutePath + "/" + split[1];
                }
                // DownloadsProvider

                if (IsDownloadsDocument(uri))
                {
                    var id = DocumentsContract.GetDocumentId(uri);
                    var contentUri = ContentUris.WithAppendedId(
                        Uri.Parse("content://downloads/public_downloads"), long.Parse(id));

                    return GetDataColumn(context, contentUri, null, null);
                }
                // MediaProvider

                if (IsMediaDocument(uri))
                {
                    var docId = DocumentsContract.GetDocumentId(uri);
                    var split = docId.Split(':');
                    var type = split[0];

                    Uri contentUri = null;
                    if ("image" == type)
                        contentUri = MediaStore.Images.Media.ExternalContentUri;
                    else if ("video" == type)
                        contentUri = MediaStore.Video.Media.ExternalContentUri;
                    else if ("audio" == type) contentUri = MediaStore.Audio.Media.ExternalContentUri;

                    const string selection = "_id=?";
                    string[] selectionArgs =
                    {
                        split[1]
                    };

                    return GetDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content" == uri.Scheme.ToLower())
            {
                // Return the remote address
                if (IsGooglePhotosUri(uri))
                    return uri.LastPathSegment;

                return GetDataColumn(context, uri, null, null);
            }
            // File
            else if ("file" == uri.Scheme.ToLower())
            {
                return uri.Path;
            }

            return null;
        }

        public static void CheckPermissions(Activity activity)
        {
            const string permission = Manifest.Permission.ReadExternalStorage;
            if (ContextCompat.CheckSelfPermission(activity, permission) != (int)Permission.Granted)
            {
                ActivityCompat.RequestPermissions(activity, new string[] { permission }, 0);
            }
        }

        private static string GetDataColumn(Context context, Uri uri, string selection,
            string[] selectionArgs)
        {
            ICursor cursor = null;
            const string column = "_data";
            string[] projection =
            {
                column
            };

            try
            {
                //if any weird exceptions get thrown here, try to give the app the right to edit files in the appmanager
                cursor = context.ContentResolver.Query(uri, projection, selection, selectionArgs,
                    null);
                if (cursor != null && cursor.MoveToFirst())
                {
                    var index = cursor.GetColumnIndexOrThrow(column);
                    return cursor.GetString(index);
                }
            }
            finally
            {
                cursor?.Close();
            }

            return null;
        }


        private static bool IsExternalStorageDocument(Uri uri)
        {
            return "com.android.externalstorage.documents" == uri.Authority;
        }


        private static bool IsDownloadsDocument(Uri uri)
        {
            return "com.android.providers.downloads.documents" == uri.Authority;
        }


        private static bool IsMediaDocument(Uri uri)
        {
            return "com.android.providers.media.documents" == uri.Authority;
        }


        private static bool IsGooglePhotosUri(Uri uri)
        {
            return "com.google.android.apps.photos.content" == uri.Authority;
        }
    }
}