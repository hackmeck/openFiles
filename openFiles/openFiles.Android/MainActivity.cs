﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Java.IO;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace openFiles.Droid
{
    [Activity(Label = "openFiles", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        private const int PickerCode = 1001;

        public static MainActivity Instance;
        private File _file;

        protected override void OnCreate(Bundle bundle)
        {
            Instance = this;
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;


            base.OnCreate(bundle);

            Forms.Init(this, bundle);
            LoadApplication(new App());
            AndroidUtil.CheckPermissions(this);
        }

        public static string GetResult()
        {
            return Instance._file?.AbsolutePath;
        }

        public static string GetName()
        {
            return Instance._file?.Name;
        }

        public static byte[] GetBytes()
        {
            return System.IO.File.ReadAllBytes(GetResult());
        }

        //opens the media chooser
        public static void OpenMediaChooser(Intent intent)
        {
            Instance.StartActivityForResult(intent, PickerCode);
        }

        //clears the choosed _myFile
        public static void ClearMsg()
        {
            Instance._file = null;
        }

        //handels what should happen if a _myFile is schoosen
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if (resultCode != Result.Ok) return;

            if (requestCode == PickerCode)
            {
                var uri = data.Data;
                var x = AndroidUtil.GetPath(ApplicationContext, uri);
                _file = new File(x);
            }
        }
    }
}